<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style type="text/css">
        table{width: 100%;}
        .tg  {border-collapse:collapse;border-color:#ccc;border-spacing:0;}
        .tg td{background-color:#fff;border-color:#ccc;border-style:solid;border-width:1px;color:#333;
          font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{background-color:#f0f0f0;border-color:#ccc;border-style:solid;border-width:1px;color:#333;
          font-family:Arial, sans-serif;font-size:14px;font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-1zbn{background-color:#ffffff;color:#000000;font-family:"Trebuchet MS", Helvetica, sans-serif !important;;text-align:left;
          vertical-align:top}
        .tg .tg-y8t1{background-color:#0e0d62;color:#ffffff;font-family:"Trebuchet MS", Helvetica, sans-serif !important;;text-align:left;
          vertical-align:top}
        .tg .tg-k9pm{background-color:#0e0d62;border-color:inherit;color:#ffffff;
          font-family:"Trebuchet MS", Helvetica, sans-serif !important;;text-align:left;text-decoration:underline;
          vertical-align:top}
    </style>
</head>
<body>
    <table class="tg">
        <thead>
            <tr>
                <th class="tg-k9pm">#</th>
                <th class="tg-y8t1">First Name</th>
                <th class="tg-y8t1">Last Name</th>
                <th class="tg-y8t1">Email</th>
                <th class="tg-y8t1">Phone</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($student as $item)
                <tr>
                    <td class="tg-1zbn" >{{ $loop->iteration }}</td>
                    <td class="tg-1zbn">{{ $item->first_name }}</td>
                    <td class="tg-1zbn">{{ $item->last_name }}</td>
                    <td class="tg-1zbn">{{ $item->email }}</td>
                    <td class="tg-1zbn">{{ $item->phone }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>