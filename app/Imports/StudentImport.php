<?php

namespace App\Imports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class StudentImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Student([
            'first_name'=>$row[1],
            'last_name'=>$row[2],
            'email'=>$row[3],
            'phone'=>$row[4]
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
