<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Exports\StudentExport;
use App\Imports\StudentImport;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{
    public function index()
    {
        $student = Student::orderBy('id','desc')->paginate(15);
        return view('student', compact('student'));
    }

    public function export_excel()
    {
        return Excel::download(new StudentExport, 'student.xlsx');
    }

    public function export_pdf()
    {
        $student = Student::all();

        $fileName = "Student.pdf";
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_top' => 10,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => 10,
        ]);

        $html = \View::make('student_pdf')->with('student', $student);
        $html = $html->render();
        $mpdf->WriteHTML($html);
        $mpdf->Output($fileName, 'I');
    }

    public function import_excel(Request $request)
    {
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
 
		$file = $request->file('file');
        $nama_file = rand().$file->getClientOriginalName();
        $file->move('file_student',$nama_file);
 
        Excel::import(new StudentImport, public_path('/file_student/'.$nama_file));
        Session::flash('sukses','Data Siswa Berhasil Diimport!');
 
        return redirect('/student');
    }
}
