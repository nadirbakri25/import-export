<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/student', [App\Http\Controllers\StudentController::class, 'index']);
Route::get('/student/export_excel', [App\Http\Controllers\StudentController::class, 'export_excel']);
Route::get('/student/export_pdf', [App\Http\Controllers\StudentController::class, 'export_pdf']);

Route::post('/student/import_excel', [App\Http\Controllers\StudentController::class, 'import_excel']);
